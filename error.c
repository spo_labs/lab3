//
// Created by Iurii Babalin on 17.01.2024.
//
#include "error.h"
#include <stdio.h>

void yyerror(const char* s){
    fprintf(stderr, "Ошибка: %s\n", s);
}