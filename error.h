//
// Created by Iurii Babalin on 17.01.2024.
//

#ifndef LAB1_ERROR_H
#define LAB1_ERROR_H

void yyerror(const char* s);
int yylex (void);

#endif //LAB1_ERROR_H
