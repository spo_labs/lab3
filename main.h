//
// Created by Iurii Babalin on 17.01.2024.
//

#ifndef LAB1_MAIN_H
#define LAB1_MAIN_H

#include <stdlib.h>
#include <stdio.h>
#include "parser.tab.h"

extern int yyparse();
extern FILE* yyin;

#endif //LAB1_MAIN_H
