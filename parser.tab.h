#include "ast.h"
/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     IDENTIFIER = 258,
     STR = 259,
     COMMA = 260,
     CHAR = 261,
     BIN = 262,
     HEX = 263,
     DEC = 264,
     TRUE = 265,
     FALSE = 266,
     IF = 267,
     ELSE = 268,
     WHILE = 269,
     DO = 270,
     BREAK = 271,
     SEMICOLON = 272,
     LPAREN = 273,
     RPAREN = 274,
     LBRACE = 275,
     RBRACE = 276,
     LBRACKET = 277,
     RBRACKET = 278,
     TYPEDEF = 279,
     RETURN = 280,
     ARRAY_COMMAS = 281,
     OR = 282,
     AND = 283,
     NOT = 284,
     MINUS = 285,
     PLUS = 286,
     PERCENT = 287,
     SLASH = 288,
     STAR = 289,
     GREATERTHANEQ = 290,
     LESSTHANEQ = 291,
     GREATERTHAN = 292,
     LESSTHAN = 293,
     NOTEQUAL = 294,
     EQUAL = 295,
     ASSIGN = 296
   };
#endif
/* Tokens.  */
#define IDENTIFIER 258
#define STR 259
#define COMMA 260
#define CHAR 261
#define BIN 262
#define HEX 263
#define DEC 264
#define TRUE 265
#define FALSE 266
#define IF 267
#define ELSE 268
#define WHILE 269
#define DO 270
#define BREAK 271
#define SEMICOLON 272
#define LPAREN 273
#define RPAREN 274
#define LBRACE 275
#define RBRACE 276
#define LBRACKET 277
#define RBRACKET 278
#define TYPEDEF 279
#define RETURN 280
#define ARRAY_COMMAS 281
#define OR 282
#define AND 283
#define NOT 284
#define MINUS 285
#define PLUS 286
#define PERCENT 287
#define SLASH 288
#define STAR 289
#define GREATERTHANEQ 290
#define LESSTHANEQ 291
#define GREATERTHAN 292
#define LESSTHAN 293
#define NOTEQUAL 294
#define EQUAL 295
#define ASSIGN 296




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 6 "parser.y"
{
    ASTNode* node;
}
/* Line 1529 of yacc.c.  */
#line 135 "parser.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

